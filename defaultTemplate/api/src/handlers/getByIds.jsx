//#import Util.js
//#import session.js
//#import $@projectCode:services/modelService.jsx
//#import @handlers/include/checklogin.jsx


;(function(){
    var spec = @spec;

    var env = checklogin();
    if(!env){
        return;
    }
    var shopId = env.shopId;
    var warehouseId = env.warehouseId;
    var m = shopId;
    var roleId = env.roleId
    var orgId = env.orgId;

    var spec = @spec;
    var tableId = 'owl_' + spec._t;
    var permissions = getPermissions(roleId,tableId);



    var params = JSON.parse($body);
    var ids = params.ids;
    var objs = [];

    for(var i=0; i<ids.length; i++){
        id = ids[i];
        var obj = @projectCodeService.get(id,true);
        if(!obj){
           continue;
        }
        var finalObj = obj;
        if(roleId!='0' || orgId!='0'){
            finalObj = JSON.parse(JSON.stringify(obj));
            filterFields(finalObj,permissions,orgId);
        }
        objs.push(finalObj);

    }

    var ret = {
        state:'ok',
        objs:objs
    }
    out.print(JSON.stringify(ret));

})();
