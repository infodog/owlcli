//#import Util.js
//#import session.js
//#import $@projectCode:services/modelService.jsx
//#import jobs.js
//#import @handlers/include/checklogin.jsx
//#import $owl_task_info:services/modelService.jsx

function main(){
    var env = checklogin();
    if(!env){
        return;
    }
    if(!env.loginId){
        //没有登录
        return;
    }
    var shopId = env.shopId;


    var params = JSON.parse($body);
    var id = params.id;
    var templateId = params.templateId;

    var now = new Date().getTime();

    var taskInfo = {
        submitDate: now,
        appId:'@projectCode',
        pageId:'tasks/exportWithTemplateTask.jsx',
        state:'notstart',
        params:JSON.stringify({
          id:id,
          templateId:templateId
        })
    };
    owl_task_infoService.add(taskInfo);
    var taskInfoId = taskInfo.id;
    JobsService.submitExportTask('@projectCode', 'tasks/exportWithTemplateTask.jsx',{
        id:id,
        templateId:templateId,
        taskInfoId:"" + taskInfoId
    },now);

    var ret = {
        state:'ok',
        taskInfoId : taskInfoId
    }

    out.print(JSON.stringify(ret));
}
main();