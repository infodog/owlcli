//#import Util.js
//#import base64.js
//#import HttpUtil.js
//#import session.js
//#import $@projectCode:services/modelService.jsx
//#import @handlers/include/checklogin.jsx


var spec = @spec;

function buildSort(sort){
    if(!sort){
        return {};
    }
    var result = [];
    for(var k in sort){
        var direction = sort[k];
        if(direction === 'ascend'){
            var sortItem = {};
            sortItem[k] = [{"order":"asc"}]
            result.push(sortItem);
        }
        else if(direction === 'descend'){
            var sortItem = {};
            sortItem[k] = [{"order":"desc"}]
            result.push(sortItem);
        }
    }
    return result;
}
;(function(){

    var env = checklogin();
    if(!env){
        return;
    }
    var shopId = env.shopId;
    var warehouseId = env.warehouseId;
    var loginId = env.loginId;
    var roleId = env.roleId;
    var orgId = env.orgId;
    var spec = @spec;
    var tableId = 'owl_' + spec._t;
    var permissions = getPermissions(roleId,tableId);
    var m = shopId;
    ///////////////////////////////

//判断是subplatform,还是shop,还是platform
    var params = JSON.parse($body);
    var searchArgs = params.searchArgs;
    var keyword = searchArgs.keyword;
    var pageSize = params.pageSize;
    var page = params.page;
    var sort = params.sort;
    var recycleBin = searchArgs.recycleBin;
    // $.log("searchArgs=" + JSON.stringify(searchArgs));
    delete searchArgs.recycleBin;
    // $.log("recycleBin=" + recycleBin);

    var effectiveSort = buildSort(sort);
    if(!page){
        page = 1;
    }
    if(!pageSize){
        pageSize=15;
    }
    pageSize = Number(pageSize);
    var from = page * pageSize-pageSize;

    var meta = spec['#meta'];
    var isPublic = meta && meta.isPublic;

    var mfilters = null;
    if(isPublic || orgId=='0'){
        delete searchArgs["_orgIds"];
    }
    else{
        searchArgs["_orgIds"] = orgId;
        mfilters =  getListPermissionFilter(tableId, roleId, orgId, loginId, permissions);
    }

   var dataSource = null;

    var isRecycleBin = false;
    if(recycleBin){
        isRecycleBin = true;
    }


   var sr =  @projectCodeService.search(mfilters, searchArgs, keyword,from, pageSize, effectiveSort, dataSource ,isRecycleBin );
   var count =  @projectCodeService.count(mfilters, searchArgs, keyword ,isRecycleBin);


   if(sr.state=='ok'){
       var ret = {
           state:'ok',
           list:sr.list,
           total:sr.total,
           count : count
       }
       out.print(JSON.stringify(ret));
   }
   else{
       var ret = {
               state:'err',
               msg:"查询出现异常"
           };
       out.print(JSON.stringify(ret));
   }





})();