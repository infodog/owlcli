//#import Util.js
//#import session.js
//#import HttpUtil.js
//#import $@projectCode:services/modelService.jsx
//#import @handlers/include/checklogin.jsx

;(function(){
    try{
        var spec = @spec;

        var env = checklogin();
        if(!env){
            return;
        }
        var shopId = env.shopId;
        var warehouseId = env.warehouseId;
        var m = shopId;


        var orgId = env.orgId;
        var roleId = env.roleId;
        var orgIds = env.orgIds;

        var params = JSON.parse($body);
        var id = params.id;
        var ids = params.ids;
        if(!ids){
            ids = [id];
        }

        var tableId = 'owl_' + spec._t;
        var permissions = getPermissions(roleId,tableId);

        for(var i=0;i<ids.length; i++){
            var id = ids[i];
            var oldObj = @projectCodeService.get(id,true);
            if(!oldObj){
                $.log("object not found:" + id);
                var ret = {
                    state: 'err',
                    msg:'object not found, id=' + id
                }
                out.print(JSON.stringify(ret));
                return;
            }
            if(roleId!=='internal'  && roleId!='0'){
                //检查是否有权限修改
                checkPermission(oldObj,permissions,orgId,"del");
            }
        }


        for(var i=0;i<ids.length; i++) {
            var id = ids[i];
        @projectCodeService.del(id);
        }


        var ret = {
            state: 'ok'
        }
        out.print(JSON.stringify(ret));
    }
    catch(e){
        var ret = {
            state:'err',
            msg:getErrorMsg(e)
        }
        out.print(JSON.stringify(ret));
    }

})();

function getErrorMsg(e){
    if(typeof e === 'string'){
        return e;
    }
    var msg = e.msg || e.message || e.toString();
    var beginIdx = msg.indexOf(':');
    if(beginIdx===-1){
        return msg;
    }
    var endIdx = msg.indexOf('(',beginIdx);
    if(endIdx===-1){
        return msg;
    }
    return msg.substring(beginIdx+1,endIdx);

}