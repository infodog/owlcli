function buildSort(sort){
    if(!sort){
        return {};
    }
    var result = [];
    for(var k in sort){
        var direction = sort[k];
        if(direction === 'ascend'){
            var sortItem = {};
            sortItem[k] = [{"order":"asc"}]
            result.push(sortItem);
        }
        else if(direction === 'descend'){
            var sortItem = {};
            sortItem[k] = [{"order":"desc"}]
            result.push(sortItem);
        }
    }
    return result;
}

function getKeywordQuery(keyword){
    if(keyword && trim(keyword).length>0){
        return "\"" + trim(keyword) + "\""
    }
    else{
        return "*"
    }
}

function getFilters(searchArgs){
    delete searchArgs.keyword;
    var filters = [];
    for(var k in searchArgs){
        var v = searchArgs[k];
        if(typeof(v)=='object' && Array.isArray(v)){
            var range={}
            range[k] = {
                'gte':v[0],
                'lte':v[1]
            }
            filters.push({range:range});
        }
        else if(typeof(v)=='object' && v.type==='terms'){
            var terms = {};
            if(v.values){
                terms[k + ".keyword"] = v.values;
                filters.push({terms:terms});
            }
        }
        else{
            var term = {};
            if(v){
                term[k+".keyword"] = trim('' + v)
                filters.push({term:term})
            }
        }
    }

    return filters;
}