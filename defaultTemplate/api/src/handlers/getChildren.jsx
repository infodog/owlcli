//#import Util.js
//#import session.js
//#import $@projectCode:services/modelService.jsx
//#import @handlers/include/checklogin.jsx
;(function() {
    var spec = @spec;
    var meta = spec['#meta'];
    var env = checklogin();
    if(!env){
        return;
    }
    var loginId = env.loginId;
    var roleId = env.roleId;
    var orgId = env.orgId;

    if(!meta.isTree){
        var ret = {
            state:'err',
            msg:meta.rem + " 不是树形结构。"
        }
        out.print(JSON.stringify(ret));
    }
    var params = JSON.parse($body);
    var parentId = params.parentId;
    var meta = spec['#meta'];
    var isPublic = meta && meta.isPublic;

    var searchArgs = {}
    if(!isPublic && orgId && orgId!=='0'){
        searchArgs["_orgIds"] = orgId;
    }
    if(parentId && parentId!=='0'){
        searchArgs['parent_id'] = parentId;
        var obj = @projectCodeService.search(null,searchArgs,null,0, 10000,null);
        var total = obj.total;
        var list = obj.list;
        var ret = {
            state : 'ok',
            total:total,
            searchArgs:searchArgs,
            list: list
        }
        out.print(JSON.stringify(ret));
    }
    else{
        var topNode = meta.root;
        if(orgId!=='0' && topNode){
            var id = topNode;
            if(topNode.indexOf("$")===0){
                var idexpr = topNode.substring(1);
                id = eval(idexpr);
            }
            var node = @projectCodeService.get(id);
            if(node){
                var ret = {
                    state : 'ok',
                    total:1,
                    list: [node]
                }
                out.print(JSON.stringify(ret));
                return;
            }
            else{
                var ret = {
                    state : 'ok',
                    total:0,
                    list: []
                }
                out.print(JSON.stringify(ret));
                return;
            }

        }
        else{
            searchArgs = {
                parent_id : "0",
            }
            if(!isPublic && orgId && orgId!='0'){
                searchArgs["_orgIds"] = orgId;
            }
            var obj = @projectCodeService.search("0",searchArgs,null,0, 10000,null);
            var total = obj.total;
            var list = obj.list;
            var ret = {
                state : 'ok',
                total:total,
                searchArgs:searchArgs,
                list: list
            }
            out.print(JSON.stringify(ret));
        }

    }



})();