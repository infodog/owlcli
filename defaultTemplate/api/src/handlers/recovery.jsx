//#import Util.js
//#import login.js
//#import user.js
//#import base64.js
//#import HttpUtil.js
//#import session.js
//#import $@projectCode:services/modelService.jsx
//#import @handlers/include/checklogin.jsx

function doRecovery(){
    var env = checklogin();
    if(!env){
        return;
    }
    var shopId = env.shopId;
    var warehouseId = env.warehouseId;
    var m = shopId;

    var params = JSON.parse($body);
    var id = params.id;

    var obj = @projectCodeService.get(id,true);
    if(obj){
        obj.del = 'F';
        var newdata = @projectCodeService.update( obj,env );
        var ret = {
            state: 'ok',
            object: newdata
        }
        out.print(JSON.stringify(ret));
    }
    else{
        var ret = {
            state: 'err',
            msg: "wrong id:"+id
        }
        out.print(JSON.stringify(ret));
        return;
    }
}
function main(){
    try{
        doRecovery();
    }
    catch(e){
        var ret = {
            state: 'err',
            msg: e
        }
        out.print(JSON.stringify(ret));
        return;
    }
}
main();