//#import Util.js
//#import file.js
//#import session.js
//#import @handlers/include/checklogin.jsx

;(function(){
    var env = checklogin();
    if(!env){
        return;
    }
    var shopId = env.shopId;
    var warehouseId = env.warehouseId;
    m = shopId;
    ///////////////////////////////

    var flattenedSpecs = @flattenedSpecs

    var fullkey = $.params.fullkey;
    var fields = flattenedSpecs.mainFields;
    var maxBytes = 1024*1024*1024;
    var exts;
    var spec;
    var size = 1;
    var unit = "G";

    for(var i=0; i<fields.length; i++){
        var field = fields[i];
        if(field.key == fullkey){
            // $.log("field=" + JSON.stringify(field));
            size = field.size;
            if(size){
                unit = size.substring(size.length-1,size.length);
                var sizeNum = size.substring(0,size.length-1);

                if(unit == 'm' || unit=='M'){
                    maxBytes = parseFloat(sizeNum) * 1024 * 1024;
                }
                else if(unit == 'k' || unit == 'K'){
                    maxBytes = 1024 * parseFloat(sizeNum);
                }

            }
            exts = field.exts;
            spec = field.spec;
        }

    }
    try{
        var fileInfo = $.uploadEx(exts,maxBytes);
        var url = FileService.getRelatedUrl(fileInfo.fileId,"300x300");
        var fullUrl = FileService.getRelatedUrl(fileInfo.fileId,null);
        var ret = {
            state:"ok",
            thumbUrl:"" + url,
            url:"" +fullUrl,
            fileId:"" +fileInfo.fileId,
            name: "" +fileInfo.fileName,
            size:fileInfo.size

        }
        out.print(JSON.stringify(ret));
    }
    catch(e){
        var ret = {
            state:"err",
            msg:"文件大小不能超过"+size + unit
        }
        out.print(JSON.stringify(ret));
    }

})();

