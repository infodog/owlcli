//#import Util.js
//#import session.js
//#import $@projectCode:services/modelService.jsx
//#import @handlers/include/checklogin.jsx

;(function(){

    var env = checklogin();
    if(!env){
        return;
    }
    var shopId = env.shopId;
    var warehouseId = env.warehouseId;
    var m = shopId;
    var roleId = env.roleId;
    var orgId = env.orgId;
    var orgIds = env.orgIds;



    ///////////////////////////////

    var flattenedSpecs = @flattenedSpecs;
    var spec = @spec;
    var tableId = 'owl_' + spec._t;
    var permissions = getPermissions(roleId,tableId);
    var formSpecs = @formSpecs;

    var ret = {
        state:'ok',
        flattenedSpecs:flattenedSpecs,
        spec:spec,
        permissions:permissions,
        orgId:orgId,
        orgIds:orgIds,
        formSpecs:formSpecs
    }
    out.print(JSON.stringify(ret));
})();

