//#import $@projectCode:services/modelService.jsx
//#import Util.js
//#import file.js



var formSpecs = @formSpecs;

var taskInfo = @projectCodeService.getExportTaskInfo( taskInfoId );
taskInfo.total = @projectCodeService.getAllSize();
taskInfo.lastTime = new Date().getTime();
taskInfo.processState = "processing";
taskInfo.msg = "获取导出总数成功......";
taskInfo.percent = "1";
taskInfo.fileUrls = [];

var from = 0;
while(from < taskInfo.total){
    var listObjs = @projectCodeService.getAllObjectsEx(from, 10000);

    taskInfo.lastTime = new Date().getTime();
    taskInfo.processState = "processing";
    taskInfo.msg = "导出数据成功，生成zip......";
    taskInfo.percent = (from + listObjs.length) * 100 / taskInfo.total;

    var s = JSON.stringify(listObjs);
    var fileId = "" + $.addAsZip(s);

    taskInfo.fileId = fileId;
    taskInfo.fileUrl = FileService.getFullPath(fileId);
    taskInfo.fileUrls.push(taskInfo.fileUrl);
    taskInfo.exportedRows = from + listObjs.length;
    from = from + listObjs.length;
    @projectCodeService.updateExportTaskInfo( taskInfoId, taskInfo );
}

taskInfo.processState = "success";
taskInfo.percent = 100;
taskInfo.msg = "导出文件成功";
@projectCodeService.updateExportTaskInfo( taskInfoId, taskInfo );









