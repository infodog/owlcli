//#import $@projectCode:services/modelService.jsx
//#import excel.js
//#import $owl_excel_templates:services/modelService.jsx
//#import $owl_task_info:services/modelService.jsx
//#import file.js

var formSpecs = @formSpecs;

function tranverseFields(formSpec, callback, ctx){
    formSpec.fields.forEach( function(field){
        if (field[ '_ft' ] == 'field') {
            callback( field, ctx );
        }
        else if (field[ '_ft' ] == 'subform') {
            var context = { parentField: field }
            tranverseFields( field, callback, context );
        }
        else if (field[ '_ft' ] == 'array') {
            var context = { parentField: field }
            tranverseFields( field, callback, context )
        }
    } );
}

function normalizeValue(value,spec){
    if(value == null){
        return null;
    }
    switch (spec.fieldType){
        case 'string':
            return 's'+value;
        case 'number':
            return 'n' + value;
        case 'date':
            try{
                var d = null;
                if(isNaN(value)){
                    d = new Date(value).getTime();
                }
                else{
                    d = new Date(Number(value)).getTime();
                }
                return 'd'+ d;
            }
            catch(e){
                return null;
            }
        case 'choice':
            for(var i=0; i<spec.options.length; i++){
                var option = spec.options[i];
                if(option[0]==value){
                    return 's'+option[1];
                }
            }
            return 's'+value;
        default:
            return "s"+value;
    }
}

function getExportDoc(obj){
    tranverseFields(formSpecs,function(f,ctx){
        if (ctx.parentField && ctx.parentField._ft == 'array') {
            var items = @projectCodeService.getValue( ctx.parentField.key, obj )
            if(items){
                for(var i=0; i<items.length; i++){
                    var item = items[i];
                    var value = item[f.origKey];
                    value = normalizeValue(value,f);
                    item[f.origKey] = value;
                }
            }
        }
        else{
            var value = @projectCodeService.getValue( f.key, obj );
            value = normalizeValue(value,f);
        @projectCodeService.setValue(f.key,value,obj);
        }
    },{});
    return obj;
}


(function(){
    var template = owl_excel_templatesService.get(templateId);
    if(!template || !template.template){
        $.log("没有配置模版,templateId=" + templateId);
        return null;
    }
    var taskInfo = owl_task_infoService.get(taskInfoId);
    taskInfo.state = "running";
    taskInfo.beginDate = new Date().getTime();
    owl_task_infoService.update(taskInfo);
    var doc = @projectCodeService.get(id);
    var exportDoc = getExportDoc(doc);
    // var templateExcelUrl = "@{@projectCode.xlsx}@";
    var templateUrl = template.template[0].url;
    var  fileId = Excel.generateExcelFromTemplate( templateUrl, exportDoc );
    if(template.toField){
        var doc = @projectCodeService.get(id);
        var url = FileService.getFullPath(fileId);
        var docName = null;
        if(typeof pname === 'undefined'){
            docName = template.name + "_" +fileId;
        }
        else{
            docName = pname;
        }
        doc[template.toField] = {
            url: "" + url,
            fileId: "" + fileId,
            name:"" + docName
        }
        @projectCodeService.update(doc);
    }

    var taskInfo = owl_task_infoService.get(taskInfoId);
    taskInfo.state = "success";
    taskInfo.percent = 100;
    taskInfo.fileId = fileId;
    taskInfo.endDate = new Date().getTime();
    taskInfo.state = "succeeded";
    taskInfo.msg = "生成Excel文件成功";
    taskInfo.fileId = fileId;

    owl_task_infoService.update(taskInfo);
})();

