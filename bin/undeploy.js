#!/usr/bin/env node

const {lstatSync, readdirSync} = require('fs')
const fs = require('fs')
const {join, resolve} = require('path')


const isDirectory = source => lstatSync(source).isDirectory()
const isFile = source => {
    try {
        return fs.statSync(source).isFile()
    } catch (e) {
        return false
    }
}


function remove(owl){
    var appId = "owl_" + owl._t;
}

function processInputDir(dir, outdir, templatePath, buildType, project) {
    readdirSync(resolve(cwd, dir)).map(name => {
        if (isDirectory(resolve(dir, name))) {
            try {
                fs.mkdirSync(resolve(outdir, name))
            } catch (e) {
            }
            processInputDir(resolve(dir, name), resolve(outdir, name), templatePath, buildType,project)
        } else if (name.indexOf('.json') > 0) {
            processOwlFile(dir, name, outdir, templatePath, buildType, dir,project)
            console.log("processed " + name);
        }
    })
}

var configFile = resolve(cwd,"owlconfig.json");
if (process.argv.length > 2) {
    configFile = resolve(cwd, process.argv[2]);
}


var content = fs.readFileSync(configFile, {encoding: 'utf-8'})
var owlconfig = JSON.parse(content)
var inPath = owlconfig.models;
var outPath = owlconfig.generatedApps
var templatePath = owlconfig.templates
var buildType = owlconfig.buildType;
var project = owlconfig.project;
buildProperties = owlconfig.buildProperties
libpath = owlconfig.libpath

var fulloutPath = resolve(cwd,outPath);

mkdir(fulloutPath);
processInputDir(inPath, fulloutPath, templatePath, buildType,project);
